**Update salt minion name**

- Stop the Salt-Minion Service (service salt-minion stop)
- Delete the current keys (rm /etc/salt/pki/minion/minion.pub and minion.pem)
- Change the minions id in /etc/salt/minion_d
- on the Salt master Delete the key (Salt-key –delete <key>)
- Restart the Salt minion service (service salt-minion start)
- on the Salt Master Accept the key (salt-key –accept-all (or key))
