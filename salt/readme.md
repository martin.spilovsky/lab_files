### Salt Mastering

# Setup

```
- Master

[centos@centos7pg_salt ~]$ cat /etc/hosts
127.0.0.1   salt localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.153.130 centos7pg_minion.vw centos7pg_minion
192.168.153.129 centos7pg_salt.vw centos7pg_salt salt

curl -L https://bootstrap.saltstack.com -o install_salt.sh
sudo sh install_salt.sh -P -M
sudo salt-key -F master

[centos@centos7pg_salt ~]$ sudo salt-key -F master
Local Keys:
master.pem:  45:c4:ed:64:dc:95:ed:51:5d:58:74:98:cc:8c:a4:65:48:a4:ee:95:e3:2c:a1:68:39:e2:df:9c:cc:48:15:df
master.pub:  1b:d8:d5:91:e0:a4:7e:4b:6e:8a:f5:27:de:a4:a4:63:44:10:04:c2:81:bb:e5:1c:9a:b3:81:c4:46:c8:1e:6d
Accepted Keys:
centos7pg_minion.vw:  b6:39:35:e7:82:8c:8d:3a:62:fe:22:67:eb:4a:3c:44:0c:b7:61:77:7c:a4:75:2f:e5:d8:d9:f9:b9:41:2e:54
centos7pg_salt.vw:  e0:af:00:df:a6:ba:be:17:f8:5d:8a:b6:14:80:88:13:57:58:9e:46:05:5b:5c:37:e7:25:0b:04:77:ad:10:85

[centos@centos7pg_salt ~]$ sudo salt-call --local key.finger
local:
    e0:af:00:df:a6:ba:be:17:f8:5d:8a:b6:14:80:88:13:57:58:9e:46:05:5b:5c:37:e7:25:0b:04:77:ad:10:85




- Minion

[centos@centos7pg_minion ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.153.130 centos7pg_minion.vw centos7pg_minion
192.168.153.129 centos7pg_salt.vw centos7pg_salt


sudo sh install_salt.sh -P
[centos@centos7pg_minion ~]$ cat /etc/salt/minion.d/minion.conf --> skip this step if you setup salt master with /etc/hosts
master: 192.168.153.129

- Both
[centos@centos7pg_minion ~]$ sudo grep master_finger /etc/salt/minion
master_finger: '1b:d8:d5:91:e0:a4:7e:4b:6e:8a:f5:27:de:a4:a4:63:44:10:04:c2:81:bb:e5:1c:9a:b3:81:c4:46:c8:1e:6d'

sudo salt-key -L

sudo salt-key -A


sudo salt-call --local key.finger

- debug
sudo salt-master -d
sudo salt-master -l debug

- restarting
sudo systemctl restart salt-master
sudo systemctl restart salt-minion

```

# Testing
```
sudo salt '*' test.ping
sudo salt '*' test.echo 'Hello'
```

# Adhoc
```
[centos@centos7pg_salt apache]$ sudo salt '*' pkg.install zsh
centos7pg_minion.vw:
    ----------
    zsh:
        ----------
        new:
            5.0.2-34.el7_8.2
        old:
centos7pg_salt.vw:
    ----------
    zsh:
        ----------
        new:
            5.0.2-34.el7_8.2
        old:

[centos@centos7pg_salt apache]$ sudo salt '*' test.ping
centos7pg_minion.vw:
    True
centos7pg_salt.vw:
    True
[centos@centos7pg_s

```

# State
```
[centos@centos7pg_salt ksh]$ cat init.sls
ksh_install:
  pkg.installed:
    - name: ksh
[centos@centos7pg_salt ksh]$ sudo salt 'centos7pg_minion.vw' state.sls ksh
centos7pg_minion.vw:
----------
          ID: ksh_install
    Function: pkg.installed
        Name: ksh
      Result: True
     Comment: All specified packages are already installed
     Started: 02:32:43.108830
    Duration: 1641.629 ms
     Changes:

Summary for centos7pg_minion.vw
------------
Succeeded: 1
Failed:    0
------------
Total states run:     1
Total run time:   1.642 s
[centos@centos7pg_salt ksh]$
```

# Grains
```
-- edit files grains.conf on minion
vi  $EDITOR /etc/salt/minion.d/grains.conf
 grains:
   roles:
     - webserver


[centos@centos7pg_salt ksh]$ sudo salt '*' grains.item os
centos7pg_salt.vw:
    ----------
    os:
        CentOS
centos7pg_minion.vw:
    ----------
    os:
        CentOS

sudo salt -G roles:webserver pkg.install hardinfo


sudo salt 'minion2' grains.set 'role' database
sudo salt 'minion*' grains.set 'env' dev
sudo salt 'minion1' grains.item role



```

# Nodegroups
```
-- edit file nodegroups.conf on master
sudo $EDITOR /etc/salt/master.d/nodegroups.conf

nodegroups:
  staging:
    - salt
    - minion1
    - minion2

sudo salt -N staging pkg.install htop

```

# Using names
```
sudo salt -C 'G@os:CentOS and E@minion*' pkg.install iotop
```


# Listing modules
```
sudo salt "master_01" sys.doc pkg.install
sudo salt "master_01" sys.argspec pkg.install
sudo salt "master_01" sys.list_modules
 sudo salt "master_01" sys.list_fuctions pkg


sudo salt '*' sys.list_modules
sudo salt '*' sys.list_functions xfs

sudo salt '*' sys.doc xfs.mkfs
sudo salt '*' sys.doc user.add


sudo salt 'salt' sys.list_functions grains
sudo salt 'salt' sys.doc grains.set
sudo salt 'salt' grains.set 'role' saltmaster
sudo salt 'minion1' grains.fetch 'role' web || sudo salt 'minion1' grains.get 'role' web
```

# Using salt
```
sudo salt 'minion2' pkg.install mariadb-server


sudo salt 'minion1' file.makedirs /var/www/dev-eski/

sudo salt 'minion1' git.config_set user.name IdunnaSoft cwd=/var/www/dev-eski
sudo salt 'minion1' git.config_set user.email webteam@idunnasoft.com cwd=/var/www/dev-eski

sudo salt 'minion1' group.add web
sudo salt 'minion2' user.add freya fullname=Freya
sudo salt 'minion1' group.adduser web thor

```
