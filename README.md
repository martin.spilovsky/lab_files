# lab_files

Some dummy files, scripts to play with Jenkins and other tools

# Windows Ubuntu WSL
```
bcdedit /set hypervisorlaunchtype off
bcdedit /set hypervisorlaunchtype autodid

dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

wsl --set-default-version 2

wsl --list --verbose

wsl --set-version <distribution name> <versionNumber>
```

# Start Vmware
```
"C:\Program Files (x86)\VMware\VMware Workstation\vmrun" start "D:\VM\Salt\Salt.vmx" nogui
"C:\Program Files (x86)\VMware\VMware Workstation\vmrun" start "D:\VM\Minion\Minion.vmx" nogui
"C:\Program Files (x86)\VMware\VMware Workstation\vmrun" start "D:\VM\Kubernetes\Kubernetes.vmx" nogui
```
